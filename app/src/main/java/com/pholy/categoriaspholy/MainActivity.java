package com.pholy.categoriaspholy;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    //DECLARACION DE CAMPOS

    private EditText mNombreCat;
    private Button mUploadPhoto;
    private ImageButton mPreviewPhoto;
    private ImageButton mPreviewPhoto2;
    private StorageReference mStorage;
    private DatabaseReference mDatabaseRef;
    private DatabaseReference mCatRef;
    private Button BotonReporte;
    private Button BotonSeleccionar;

    private Uri mImageUri;
    private Uri mImageUriSelect;

    String catId;
    String catName;
    String catImageFeed;
    String catImageSelect;

    private ProgressDialog mProgressDialog;

    private final int REQUEST_CODE_GALLERY = 1;
    private final int REQUEST_CODE_GALLERY_SELECT = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_main);

        //INICIALIZACION DE CAMPOS

        mDatabaseRef = FirebaseDatabase.getInstance().getReference();

        mCatRef = mDatabaseRef.child("categorias");

        mStorage = FirebaseStorage.getInstance().getReference().child("Category_Images");

        mProgressDialog = new ProgressDialog(this);


        mNombreCat = (EditText) findViewById(R.id.nCategoria);
        mPreviewPhoto = (ImageButton) findViewById(R.id.mPreviewPhoto);
        mUploadPhoto = (Button) findViewById(R.id.subirfoto);
        mPreviewPhoto2 = (ImageButton) findViewById(R.id.mPreviewPhoto2);


        BotonSeleccionar = (Button) findViewById(R.id.mSeleccionarFoto);

        BotonSeleccionar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                startActivityForResult(intent, REQUEST_CODE_GALLERY_SELECT);
            }
        });


        BotonReporte = (Button) findViewById(R.id.mReportBtn);


        BotonReporte.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(MainActivity.this, ReportesActivity.class);
                startActivity(intent);

            }
        });


        mUploadPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                startActivityForResult(intent, REQUEST_CODE_GALLERY);
            }
        });


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK && requestCode == REQUEST_CODE_GALLERY) {

            mImageUri = data.getData();

            mPreviewPhoto.setImageURI(mImageUri);

        }

        if (resultCode == RESULT_OK && requestCode == REQUEST_CODE_GALLERY_SELECT) {

            mImageUriSelect = data.getData();

            mPreviewPhoto2.setImageURI(mImageUriSelect);

        }

    }

    //Click desde el toolbar para subir

    public void subir(MenuItem item){

        catName = mNombreCat.getText().toString();

        if (!TextUtils.isEmpty(catName) && mImageUri != null && mImageUriSelect != null) {

            catId = mCatRef.push().getKey();

            mProgressDialog.setMessage("Creando categoria...");
            mProgressDialog.show();

            StorageReference imagePath = mStorage.child(catName);
            final StorageReference imagePath2 = mStorage.child(catName + "_select");

            imagePath.putFile(mImageUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                    final Uri url = taskSnapshot.getDownloadUrl();

                    catImageFeed = url.toString();

                    imagePath2.putFile(mImageUriSelect).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                            final Uri url = taskSnapshot.getDownloadUrl();

                            catImageSelect = url.toString();

                            Categoria c = new Categoria(catId, catName, catImageFeed, catImageSelect);

                            Map<String, Object> catValues = c.toMap();

                            Map<String, Object> childUpdates = new HashMap<>();

                            childUpdates.put("/categorias/"+catId, catValues);
                            childUpdates.put("/timer/"+catId, Calendar.getInstance().getTimeInMillis());

                            mDatabaseRef.updateChildren(childUpdates).addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {
                                    mProgressDialog.dismiss();

                                    Toast.makeText(MainActivity.this, "Categoria creada correctamente", Toast.LENGTH_SHORT).show();

                                    mNombreCat.setText("");
                                    mPreviewPhoto.setImageURI(null);
                                    mPreviewPhoto2.setImageURI(null);
                                }
                            });

                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Toast.makeText(MainActivity.this, "OCURRIO UN ERROR AL SUBIR LA IMAGEN DEL FOTO SELECT CATEGORY."+e.getMessage(), Toast.LENGTH_SHORT).show();
                            mProgressDialog.dismiss();
                        }
                    });



                }
            }).addOnFailureListener(new OnFailureListener() {


                @Override
                public void onFailure(@NonNull Exception e) {

                    Toast.makeText(MainActivity.this, "OCURRIO UN ERROR AL SUBIR LA IMAGEN DEL FEED CATEGORY."+e.getMessage(), Toast.LENGTH_SHORT).show();
                    mProgressDialog.dismiss();
                }
            });


        } else {
            Snackbar.make(getCurrentFocus(), "Debe ingresar una descripcion y una imagen de la categoria.", Snackbar.LENGTH_LONG).show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_subir) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}