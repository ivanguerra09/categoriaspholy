package com.pholy.categoriaspholy;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class ReportesActivity extends AppCompatActivity {

    private RecyclerView mfeedReportes;

    private DatabaseReference mDatabaseReportes;
    private DatabaseReference mDatabaseFotos;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reportes);

        mDatabaseReportes = FirebaseDatabase.getInstance().getReference().child("reportes");
        mDatabaseFotos = FirebaseDatabase.getInstance().getReference().child("Fotos");
        mfeedReportes = (RecyclerView) findViewById(R.id.feed_reportes);

        mfeedReportes.setHasFixedSize(true);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        mfeedReportes.setLayoutManager(layoutManager);
        RecyclerView.ItemAnimator itemAnimator = new DefaultItemAnimator();
        itemAnimator.setAddDuration(1000);
        itemAnimator.setRemoveDuration(1000);
        mfeedReportes.setItemAnimator(itemAnimator);

    }

    public void deletePost(final String report_key) {

        mDatabaseReportes.child(report_key).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String post_key = dataSnapshot.getValue(String.class);

                mDatabaseFotos.child(post_key).removeValue();
                mDatabaseReportes.child(report_key).removeValue();

                Toast.makeText(ReportesActivity.this, "El post fue borrado con exito!", Toast.LENGTH_LONG).show();

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    public AlertDialog createDeleteDialog(final String report_key) {
        AlertDialog.Builder builder = new AlertDialog.Builder(ReportesActivity.this);

        builder.setTitle("Reportar Post")
                .setMessage("¿Está seguro que desea eliminar el post?")
                .setPositiveButton("SI",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                deletePost(report_key);

                            }
                        })
                .setNegativeButton("NO",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });

        return builder.create();
    }

    public void denyReport(final String report_key) {

        mDatabaseReportes.child(report_key).removeValue();
        Toast.makeText(ReportesActivity.this, "REPORTE ELIMINADO!", Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onStart() {
        super.onStart();

        final FirebaseRecyclerAdapter<String, PostViewHolder> firebaseRecyclerAdapter = new FirebaseRecyclerAdapter<String, PostViewHolder>(

                String.class,
                R.layout.feed_row,
                PostViewHolder.class,
                mDatabaseReportes

        ) {
            @Override
            protected void populateViewHolder(final PostViewHolder viewHolder, String model, final int position) {

                final DatabaseReference ref = getRef(position);

                viewHolder.setPostUsername(ref.getKey());
                viewHolder.setImage(ReportesActivity.this, ref.getKey());

                viewHolder.mAceptarBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        createDeleteDialog(ref.getKey()).show();
                    }
                });

                viewHolder.mBorrarBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        denyReport(ref.getKey());
                    }
                });


            }
        };

        mfeedReportes.setAdapter(firebaseRecyclerAdapter);

    }

    public static class PostViewHolder extends RecyclerView.ViewHolder {

        View mView;
        DatabaseReference mDatabaseReportes;
        DatabaseReference mDatabaseFotos;

        TextView post_username;
        ImageView post_image;
        ImageButton mAceptarBtn;
        ImageButton mBorrarBtn;

        public PostViewHolder(View itemView) {
            super(itemView);

            mView = itemView;


            mDatabaseFotos = FirebaseDatabase.getInstance().getReference().child("Fotos");

            mDatabaseReportes = FirebaseDatabase.getInstance().getReference().child("reportes");

            mDatabaseReportes.keepSynced(true);

            post_username = (TextView) mView.findViewById(R.id.post_username);

            post_image = (ImageView) mView.findViewById(R.id.post_image);

            mAceptarBtn = (ImageButton) mView.findViewById(R.id.mAceptarBtn);

            mBorrarBtn = (ImageButton) mView.findViewById(R.id.mBorrarBtn);
        }

        //seteamos el nombre de usuario
        public void setPostUsername(final String id) {

            mDatabaseReportes.child(id).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    String post_id = dataSnapshot.getValue(String.class);

                    mDatabaseFotos.child(post_id).addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {

                            String username = dataSnapshot.child("username").getValue(String.class);

                            post_username.setText(username);

                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

        }

        public void setImage(final Context ctx, final String id) {

            mDatabaseReportes.child(id).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    String post_id = dataSnapshot.getValue(String.class);

                    mDatabaseFotos.child(post_id).addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {

                            String image = dataSnapshot.child("imagen").getValue(String.class);

                            Glide
                                .with(ctx)
                                .load(image)
                                .thumbnail(0.2f)
                                .into(post_image);

                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

        }


    }


}