package com.pholy.categoriaspholy;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Ivan on 29/10/2016.
 */
public class Categoria {

    private String category_id;
    private String category_name;
    private String category_feed_image;
    private String category_select_image;
    private Long category_timer;

    public Categoria() {

    }

    public Categoria(String category_id, String category_name, String category_feed_image, String category_select_image) {
        this.category_id = category_id;
        this.category_name = category_name;
        this.category_feed_image = category_feed_image;
        this.category_select_image = category_select_image;
        this.category_timer = Calendar.getInstance().getTimeInMillis();
    }

    public Map<String, Object> toMap() {

        HashMap<String, Object> result = new HashMap<>();

        result.put("category_id", category_id);
        result.put("category_name", category_name);
        result.put("category_feed_image", category_feed_image);
        result.put("category_select_image", category_select_image);
        result.put("category_timer", category_timer);

        return result;
    }

}