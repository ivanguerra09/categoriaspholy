package com.pholy.categoriaspholy;

/**
 * Created by Ivan on 07/11/2016.
 */
public class Post {

    private String username;
    private String imagen;

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

}